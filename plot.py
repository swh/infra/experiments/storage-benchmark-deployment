import re

from matplotlib.figure import Figure
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas

_data_re = re.compile(r' *'
                      r'(?P<nb_lines>[0-9.]+[kM]) '
                      r'(?P<time>[0-9:]+) '
                      r'\[ *(?P<instant_rate>[^\]]+)\] '
                      r'\[ *(?P<avg_rate>[0-9.]+)k/s\]')

def parse_SI(s):
    """Parses a float with an SI suffix (k, M, G, ...)."""
    if set(s) <= set('1234567890.'):
        return float(s)
    elif s.endswith('k'):
        return float(s[0:-1])*1000
    elif s.endswith('M'):
        return float(s[0:-1])*1000000
    else:
        raise NotImplementedError(s)

def parse_time(s):
    (hours, minutes, seconds) = s.split(':')
    return int(hours)*3600 + int(minutes)*60 + int(seconds)

def plot_chunk(ax, chunk):
    (first_line, data) = chunk.split('\n', 1)
    label = first_line.split(' | ')[0]
    if 'bad' in label:
        return
    data = list(map(_data_re.match, data.split('\n')))
    data = [(1./parse_SI(x.group('avg_rate')), parse_SI(x.group('nb_lines'))/1000000)
            for x in data]
    (data_y, data_x) = zip(*data)
    if 'cassandra' in label:
        linestyle = '--'
    elif 'pgsql' in label:
        linestyle = '-'
    ax.plot(data_x, data_y, label=label, linestyle=linestyle)

def main():
    with open('plot_data.txt') as fd:
        chunks = fd.read().strip().split('\n\n')
    fig = Figure(figsize=(15,10))
    FigureCanvas(fig)
    ax = fig.add_subplot(111)
    for chunk in chunks:
        plot_chunk(ax, chunk)
    ax.set_xlabel('table size (millions of rows)')
    ax.set_ylabel('avg time per 1000 insertion (s)')
    ax.set_title('performance of revision_add (with no parents nor duplicates)')
    legend = ax.legend(loc='lower right', shadow=True)
    fig.savefig('/tmp/benchmark.png')
    ax.set_title('')

if __name__ == '__main__':
    main()
