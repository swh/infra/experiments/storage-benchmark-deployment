##############################
# Global

provider "azurerm" {
}

data "azurerm_network_security_group" "worker-nsg" {
  name                = "worker-nsg"
  resource_group_name = "swh-resource"
}

data "azurerm_subnet" "default" {
  name                 = "default"
  virtual_network_name = "swh-vnet"
  resource_group_name  = "swh-resource"
}

##############################
# Cassandra servers

resource "azurerm_network_interface" "cassandra-servers_interfaces" {
  count = "${var.nb_cassandra_servers}"

  name                = "cassandra-server-${count.index}_interface"
  location            = "westeurope"
  resource_group_name = "euwest-cassandra"
  network_security_group_id = "${data.azurerm_network_security_group.worker-nsg.id}"

  ip_configuration {
    name                          = "myNicConfiguration"
    subnet_id                     = "${data.azurerm_subnet.default.id}"
    public_ip_address_id          = ""
    private_ip_address_allocation = "Dynamic"
  }
}

resource "azurerm_virtual_machine" "cassandra-servers" {
  count = "${var.nb_cassandra_servers}"

  name                  = "cassandra-server-${count.index}"
  location              = "westeurope"
  resource_group_name   = "euwest-cassandra"
  network_interface_ids = [
    "${element(azurerm_network_interface.cassandra-servers_interfaces.*.id, count.index)}"]
  vm_size               = "Standard_DS2_v2"

  storage_os_disk {
    name              = "cassandra-server-${count.index}_osdisk"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Premium_LRS"
  }

  storage_image_reference {
    publisher = "credativ"
    offer     = "Debian"
    sku       = "9"
    version   = "latest"
  }

  os_profile {
    computer_name  = "test-cassandra-server-${count.index}"
    admin_username = "vlorentz"
  }
  os_profile_linux_config {
    disable_password_authentication = true
    ssh_keys {
      path = "/home/vlorentz/.ssh/authorized_keys"
      key_data = "${var.ssh_key_data}"
    }
  }

  storage_data_disk {
    name = "cassandra-server-${count.index}_datadisk0"
    lun = 0
    disk_size_gb = 64
    managed_disk_type = "Premium_LRS"
    create_option = "Empty"
  }
  storage_data_disk {
    name = "cassandra-server-${count.index}_datadisk1"
    lun = 1
    disk_size_gb = 64
    managed_disk_type = "Premium_LRS"
    create_option = "Empty"
  }
  storage_data_disk {
    name = "cassandra-server-${count.index}_datadisk2"
    lun = 2
    disk_size_gb = 64
    managed_disk_type = "Premium_LRS"
    create_option = "Empty"
  }
  storage_data_disk {
    name = "cassandra-server-${count.index}_datadisk3"
    lun = 3
    disk_size_gb = 64
    managed_disk_type = "Premium_LRS"
    create_option = "Empty"
  }
}

output "cassandra_servers_ips" {
  value = "${azurerm_network_interface.cassandra-servers_interfaces.*.private_ip_address}"
}

##############################
# Postgresql servers

resource "azurerm_network_interface" "pgsql-servers_interfaces" {
  count = "${var.nb_pgsql_servers}"

  name                = "pgsql-servers-${count.index}_interface"
  location            = "westeurope"
  resource_group_name = "euwest-cassandra"
  network_security_group_id = "${data.azurerm_network_security_group.worker-nsg.id}"

  ip_configuration {
    name                          = "myNicConfiguration"
    subnet_id                     = "${data.azurerm_subnet.default.id}"
    public_ip_address_id          = ""
    private_ip_address_allocation = "Dynamic"
  }
}

resource "azurerm_virtual_machine" "pgsql-servers" {
  count = "${var.nb_pgsql_servers}"

  name                  = "pgsql-server-${count.index}"
  location              = "westeurope"
  resource_group_name   = "euwest-cassandra"
  network_interface_ids = [
    "${element(azurerm_network_interface.pgsql-servers_interfaces.*.id, count.index)}"]
  vm_size               = "Standard_DS2_v2"

  storage_os_disk {
    name              = "pgsql-server-${count.index}_osdisk"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Premium_LRS"
  }

  storage_image_reference {
    publisher = "credativ"
    offer     = "Debian"
    sku       = "9"
    version   = "latest"
  }

  os_profile {
    computer_name  = "test-pgsql-server-${count.index}"
    admin_username = "vlorentz"
  }
  os_profile_linux_config {
    disable_password_authentication = true
    ssh_keys {
      path = "/home/vlorentz/.ssh/authorized_keys"
      key_data = "${var.ssh_key_data}"
    }
  }

  storage_data_disk {
    name = "pgsql-server-${count.index}_datadisk0"
    lun = 0
    disk_size_gb = 64
    managed_disk_type = "Premium_LRS"
    create_option = "Empty"
  }
  storage_data_disk {
    name = "pgsql-server-${count.index}_datadisk1"
    lun = 1
    disk_size_gb = 64
    managed_disk_type = "Premium_LRS"
    create_option = "Empty"
  }
  storage_data_disk {
    name = "pgsql-server-${count.index}_datadisk2"
    lun = 2
    disk_size_gb = 64
    managed_disk_type = "Premium_LRS"
    create_option = "Empty"
  }
  storage_data_disk {
    name = "pgsql-server-${count.index}_datadisk3"
    lun = 3
    disk_size_gb = 64
    managed_disk_type = "Premium_LRS"
    create_option = "Empty"
  }
}

output "pgsql_servers_ips" {
  value = "${azurerm_network_interface.pgsql-servers_interfaces.*.private_ip_address}"
}


##############################
# Client

resource "azurerm_network_interface" "cassandra-clients_interfaces" {
  count = "${var.nb_clients}"

  name                = "cassandra-client-${count.index}_interface"
  location            = "westeurope"
  resource_group_name = "euwest-cassandra"
  network_security_group_id = "${data.azurerm_network_security_group.worker-nsg.id}"

  ip_configuration {
    name                          = "myNicConfiguration"
    subnet_id                     = "${data.azurerm_subnet.default.id}"
    public_ip_address_id          = ""
    private_ip_address_allocation = "Dynamic"
  }
}

resource "azurerm_virtual_machine" "cassandra-clients" {
  count = "${var.nb_clients}"

  name                  = "cassandra-client-${count.index}"
  location              = "westeurope"
  resource_group_name   = "euwest-cassandra"
  network_interface_ids = [
    "${element(azurerm_network_interface.cassandra-clients_interfaces.*.id, count.index)}"]
  vm_size               = "Standard_B4ms"

  storage_os_disk {
    name              = "cassandra-client-${count.index}_osdisk"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Premium_LRS"
  }

  storage_image_reference {
    publisher = "credativ"
    offer     = "Debian"
    sku       = "9"
    version   = "latest"
  }

  os_profile {
    computer_name  = "test-cassandra-client-${count.index}"
    admin_username = "vlorentz"
  }
  os_profile_linux_config {
    disable_password_authentication = true
    ssh_keys {
      path = "/home/vlorentz/.ssh/authorized_keys"
      key_data = "${var.ssh_key_data}"
    }
  }

  storage_data_disk {
    name = "cassandra-client-${count.index}_datadisk0"
    lun = 0
    disk_size_gb = 256
    managed_disk_type = "Premium_LRS"
    create_option = "Empty"
  }
  storage_data_disk {
    name = "cassandra-client-${count.index}_datadisk1"
    lun = 1
    disk_size_gb = 256
    managed_disk_type = "Premium_LRS"
    create_option = "Empty"
  }
}

output "cassandra_clients_ips" {
  value = "${azurerm_network_interface.cassandra-clients_interfaces.*.private_ip_address}"
}

